import os.path
from difflib import get_close_matches
from typing import Optional, List, Any

from counter import Counter, Collection
from csv_file import CsvFile
from display_manager import DisplayManager
from entry import Question, Entry
from file_manager import FileManager
from utils import count_digits


class Action:
    text: str = ''
    content_changed: bool = False

    def __str__(self):
        return self.text

    def start(self):
        pass


class SeeCollectionDetails(Action):
    def __init__(self, collection: Collection):
        super().__init__()
        self.collection: Collection = collection

    def start(self):
        top_labels: List = self.collection.top_labels()
        digits = count_digits(top_labels[0][1])
        labels = []
        for label, count in top_labels:
            sources = self.collection.sources_for_label(label)
            label_text = f'{str(count).rjust(digits)}: {label} : in {", ".join([str(src) for src in sources])}'
            labels.append(label_text)
        DisplayManager.message(f'Answer group: {self.collection.label}. Total: {self.collection.count}', labels)


class EditCollection(Action):
    def __init__(self, collection: Collection):
        super().__init__()
        self.collection: Collection = collection

    def start(self):
        while True:
            top_labels: List = self.collection.top_labels()
            digits = count_digits(top_labels[0][1])
            labels = [f'{str(count).rjust(digits)}: {label}' for label, count in top_labels]
            header = f'Merge items from collection: {self.collection.label} (from to)'
            a, b = DisplayManager.pick_two_items(header, labels)
            if not a:
                return
            self.collection.merge_labels(top_labels[a-1][0], top_labels[b-1][0])


class DeleteCollection(Action):
    def __init__(self, collection: Collection):
        super().__init__()
        self.collection: Collection = collection

    def start(self):
        top_labels: List = self.collection.top_labels()
        digits = count_digits(top_labels[0][1])
        labels = [f'{str(count).rjust(digits)}: {label}' for label, count in top_labels]
        question = f'Are you sure you want to clear all items from collection \'{self.collection.label}\'?'
        ret = DisplayManager.simple_choice_with_list(question, labels, 'yn')
        if ret == 'y':
            self.collection.clear_all()
            self.content_changed = True


class AskMergeCollections(Action):
    def __init__(self, csv_file: CsvFile, counter: Counter, source: Collection, destination: Collection):
        self.csv_file: CsvFile = csv_file
        self.counter: Counter = counter
        self.source: Collection = source
        self.destination: Collection = destination

    def start(self):
        question = f'Are you sure you want to merge \'{self.source.label}\' into \'{self.destination.label}\'. [y/n/(i)nfo/(e)ntries]'
        while True:
            res = DisplayManager.simple_choice_with_list(question, self.source.top_labels(), 'ynie', auto_options=False)
            if res == 'y':
                self.counter.slugs_into_value(self.source.slug, self.destination.label)
                self.content_changed = True
                break
            elif res == 'i':
                SeeCollectionDetails(self.source).start()
            elif res == 'e':
                entries = set()
                for answer in self.source.answers:
                    entries.add(answer.row)
                entries = list(entries)
                SelectEntry(self.csv_file, entries).start()
            else:
                return


class FindSimilarInAll(Action):
    def __init__(self, csv_file: CsvFile, counter: Counter, picked: Collection):
        self.csv_file: CsvFile = csv_file
        self.counter: Counter = counter
        self.picked: Collection = picked

    def start(self):
        text = f'Matches similar to {self.picked.label}. Select to merge'
        slugs = [slug for slug in self.counter.state.keys() if slug != self.picked.slug]
        matches = get_close_matches(self.picked.slug, slugs, 5000, cutoff=.4)
        collections = [self.counter[slug] for slug in matches]
        while True:
            index = DisplayManager.pick_item(text, collections)
            if index == 0:
                return
            picked_collection = collections[index - 1]
            AskMergeCollections(self.csv_file, self.counter, picked_collection, self.picked).start()


class MostCommonInAll(Action):
    text: str = 'Count most common in all.'

    def __init__(self, csv_file: CsvFile):
        super().__init__()
        self.csv_file: CsvFile = csv_file

    def start(self):
        counter: Counter = Counter()
        counter.count_entries(self.csv_file.entries, open_only=True)

        top: List[Collection] = counter.top()

        while True:
            pos, action = DisplayManager.pick_action_with_item(
                'Top most common answers. Pick number and action (s)ee more, (e)dit, (d)elete, (f)ind similar', 'sedf', top)
            item: Collection = top[pos - 1]
            slug: str = item.slug  # As item can change, we need separate variable for slug at the time

            if action == 's':
                chosen_action: Action = SeeCollectionDetails(item)
            elif action == 'e':
                chosen_action: Action = EditCollection(item)
            elif action == 'd':
                chosen_action: Action = DeleteCollection(item)
            elif action == 'f':
                chosen_action: Action = FindSimilarInAll(self.csv_file, counter, item)
            else:
                return

            chosen_action.start()
            if chosen_action.content_changed:
                counter.update(slug)
                top = counter.top()


class DeleteFromQuestion(Action):
    def __init__(self, question: Question):
        self.question: Question = question

    def start(self):
        while True:
            text = 'Select entry to delete'
            index = DisplayManager.pick_item(text, self.question.top())
            if index:
                text = 'Are you sure you want to remove all answers from this list'
                collection = self.question.top()[index - 1]
                top_labels = collection.top_labels()
                digits = count_digits(top_labels[0][1])
                labels = [f'{str(count).rjust(digits)}: {label}' for label, count in top_labels]
                confirm = DisplayManager.simple_choice_with_list(text, labels, 'yn')
                if confirm == 'y':
                    collection.clear_all()
            else:
                return


class MergeFromQuestion(Action):
    def __init__(self, question: Question):
        self.question: Question = question

    def start(self):
        text = 'Select entries to merge (a into b)'
        top: List[Collection] = self.question.top()
        while True:
            a, b = DisplayManager.pick_two_items(text, top)
            if not a:
                return
            col_a = top[a - 1]
            col_b = top[b - 1]
            res = DisplayManager.simple_choice(f'Merge \'{col_a.label}\' into \'{col_b.label}\'', 'yn')
            if res == 'y':
                self.question.slugs_into_value(col_a.slug, col_b.label)
                top: List[Collection] = self.question.top()


class FindSimilarTo(Action):
    def __init__(self, csv_file: CsvFile, question: Question, picked: Collection):
        self.csv_file: CsvFile = csv_file
        self.question: Question = question
        self.picked: Collection = picked

    def start(self):
        text = f'Matches similar to {self.picked.label}. Select to merge'
        slugs = [slug for slug in self.question.counter.state.keys() if slug != self.picked.slug]
        matches = get_close_matches(self.picked.slug, slugs, 5000, cutoff=.4)
        collections = [self.question.counter[slug] for slug in matches]
        while True:
            index = DisplayManager.pick_item(text, collections)
            if index == 0:
                return
            picked_collection = collections[index - 1]
            AskMergeCollections(self.csv_file, self.question.counter, picked_collection, self.picked).start()


class FindSimilarEverywhere(Action):
    def __init__(self, csv_file: CsvFile, picked: Collection):
        self.csv_file: CsvFile = csv_file
        self.picked: Collection = picked

    def start(self):
        text = f'Matches similar to {self.picked.label}. Select to merge'
        counter: Counter = Counter()
        counter.count_entries(self.csv_file.entries, open_only=True)
        slugs = [slug for slug in counter.state.keys() if slug != self.picked.slug]
        matches = get_close_matches(self.picked.slug, slugs, 5000, cutoff=.4)
        collections = [counter[slug] for slug in matches]
        while True:
            index = DisplayManager.pick_item(text, collections)
            if index == 0:
                return
            picked_collection = collections[index - 1]
            AskMergeCollections(self.csv_file, counter, picked_collection, self.picked).start()


class FindSimilarEverywhereFromQuestion(Action):
    def __init__(self, csv_file: CsvFile, question: Question):
        self.csv_file: CsvFile = csv_file
        self.question: Question = question

    def start(self):
        text = 'Select entry fo find similar everywhere'
        top: List[Collection] = self.question.top()
        while True:
            index = DisplayManager.pick_item(text, top)
            if index == 0:
                return
            collection = top[index - 1]
            FindSimilarEverywhere(self.csv_file, collection).start()


class FindSimilarFromQuestion(Action):
    def __init__(self, csv_file: CsvFile, question: Question):
        self.csv_file: CsvFile = csv_file
        self.question: Question = question

    def start(self):
        text = 'Select entry fo find similar'
        top: List[Collection] = self.question.top()
        while True:
            index = DisplayManager.pick_item(text, top)
            if index == 0:
                return
            collection = top[index - 1]
            FindSimilarTo(self.csv_file, self.question, collection).start()


class ShowQuestion(Action):
    def __init__(self, csv_file: CsvFile, question: Question):
        self.csv_file: CsvFile = csv_file
        self.question: Question = question

    def start(self):
        while True:
            header = f'Top Answers for \'{self.question.title}\' (d)elete, (m)erge, (f)ind similar, find (e)verywhere'
            res = DisplayManager.simple_choice_with_list(header, self.question.top(), 'dmfe', auto_options=False,
                                                         numerate=False)
            if res == 'd':
                DeleteFromQuestion(self.question).start()
            elif res == 'm':
                MergeFromQuestion(self.question).start()
            elif res == 'f':
                FindSimilarFromQuestion(self.csv_file, self.question).start()
            elif res == 'e':
                FindSimilarEverywhereFromQuestion(self.csv_file, self.question).start()
            else:
                return


class SelectQuestion(Action):
    text = 'Select question.'

    def __init__(self, csv_file: CsvFile):
        super().__init__()
        self.csv_file: CsvFile = csv_file

    def start(self):
        while True:
            index = DisplayManager.pick_item('Select question', self.csv_file.questions)
            if index == 0:
                return
            question: Question = self.csv_file.questions[index - 1]
            ShowQuestion(self.csv_file, question).start()


class ShowEntry(Action):
    def __init__(self, entry: Entry):
        self.entry: Entry = entry
        self.remove_entry: bool = False

    def start(self):
        remove = DisplayManager.simple_choice_with_list("Remove whole entry", self.entry.answers, 'yn')
        if remove == 'y':
            self.remove_entry = True


class SelectEntry(Action):
    text = 'Select entry.'

    def __init__(self, csv_file: CsvFile, entries: Optional[List[Entry]] = None):
        self.csv_file: CsvFile = csv_file
        self.entries: List[Entry] = entries if entries is not None else csv_file.entries

    def start(self):
        while True:
            index = DisplayManager.pick_item('Select entry', self.entries)
            if index == 0:
                return
            entry = self.entries[index - 1]
            show_entry_action = ShowEntry(entry)
            show_entry_action.start()
            if show_entry_action.remove_entry:
                self.csv_file.remove_entry(entry)


class SusEntries(Action):
    text = "Find sus entries."

    def __init__(self, csv_file: CsvFile):
        self.csv_file: CsvFile = csv_file

    def start(self):
        while True:
            entries = sorted(self.csv_file.entries, key=lambda e: e.non_empty)
            labels = [f'{entry.non_empty} filled, {entry}' for entry in entries]
            index = DisplayManager.pick_item('Select entry', labels)
            if index == 0:
                return
            entry = entries[index - 1]
            show_entry_action = ShowEntry(entry)
            show_entry_action.start()
            if show_entry_action.remove_entry:
                self.csv_file.remove_entry(entry)


class SaveCSV(Action):
    text = 'Save csv.'

    def __init__(self, csv_file: CsvFile):
        super().__init__()
        self.csv_file: CsvFile = csv_file

    def start(self):
        org_name, _ = os.path.splitext(os.path.basename(self.csv_file.file_name))
        default = f'{org_name}_new.csv'
        name = DisplayManager.text_input('Where to save csv file?', default)
        if name:
            FileManager.save_csv(name, self.csv_file.to_plain_data())


class ActionMenu:
    def __init__(self, actions: List[Action], add_exit='last'):
        self.parent: Optional[ActionMenu]
        self.actions: List[Action] = actions
        self.add_exit: str = add_exit

    def start(self):
        actions: List[Any] = self.actions.copy()
        exit_option: int = -1
        if self.add_exit == 'last':
            actions.append('Exit')
            exit_option = len(actions)
        while True:
            option = DisplayManager.pick_action(actions)
            if option == exit_option:
                return
            if option != 0:
                self.actions[option - 1].start()
