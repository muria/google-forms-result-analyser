from typing import List

from entry import Entry, Answer, Question
from file_manager import FileManager
from utils import col_to_index


class QuestionSelector:
    def __init__(self, selector: str):
        self.indexes = set()  # Since column A is always a timestamp we start indexing questions from B = 0
        self.always_true: bool = not selector
        if self.always_true:
            return
        for selection in selector.split(' '):
            if '-' in selection:
                first, last = selection.split('-')
                first = col_to_index(first)
                last = col_to_index(last)
                for i in range(first, last + 1):
                    self.indexes.add(i)
            else:
                self.indexes.add(col_to_index(selection))

    def __contains__(self, item):
        return item in self.indexes


class CsvFile:
    def __init__(self, file_name: str, rows: List[List[str]], open_questions: str = '', sus_questions: str = '',
                 auto_title: str = ''):
        self.file_name: str = file_name
        self.header: List[str] = rows[0]
        self.entries: List[Entry] = []
        self.questions: List[Question] = []
        self.open_questions = QuestionSelector(open_questions)
        self.sus_questions = QuestionSelector(sus_questions)
        self.auto_titles = QuestionSelector(auto_title)
        for i, col_header in enumerate(self.header[1:]):
            self.questions.append(Question(col_header, i in self.open_questions, i in self.sus_questions, i in self.auto_titles))
        for row in rows[1:]:
            entry: Entry = Entry(row[0])
            self.entries.append(entry)
            for cell, col in zip(row[1:], self.questions):
                answer = Answer(cell)
                entry.add(answer)
                col.add(answer)

    def remove_entry(self, entry: Entry):
        self.entries.remove(entry)
        for answer in entry.answers:
            question: Question = answer.column
            question.remove_answer(answer)

    def to_plain_data(self) -> List[List[str]]:
        data = [self.header]
        for entry in self.entries:
            data.append(entry.to_string_list())
        return data

    @classmethod
    def open_from_path(cls, path: str, open_questions: str = '', sus_questions: str = '', auto_title: str = ''):
        csv_file = FileManager.open_csv(path)
        return cls(path, csv_file, open_questions, sus_questions, auto_title)
