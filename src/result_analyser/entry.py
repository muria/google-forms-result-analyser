from typing import List, Optional

from slugify import slugify


class Answer:
    def __init__(self, value: str):
        self.original_value: str = value.strip()
        self._value: str = self.original_value
        self.row: Optional['Entry'] = None
        self.column: Optional['Question'] = None

    @property
    def value(self):
        return self._value

    @value.setter
    def value(self, val: str):
        self._value = val
        if self.column:
            self.column.clear_counter()
        if self.row:
            self.row.reset_non_empty_counter()

    @property
    def is_open(self) -> bool:
        return self.column is None or self.column.is_open

    @property
    def is_sus(self) -> bool:
        return self.column is None or self.column.is_sus

    @property
    def slug_name(self) -> str:
        return slugify(self.value)

    def empty(self) -> bool:
        return not bool(self.value)

    def __str__(self):
        return self.value


class Entry:
    def __init__(self, timestamp: str):
        self.timestamp: str = timestamp
        self.answers = []
        self._non_empty: int = -1

    def __iter__(self):
        return iter(self.answers)

    def __getitem__(self, item):
        return self.answers[item]

    @property
    def non_empty(self):
        if self._non_empty == -1:
            self._non_empty = 0
            for answer in self.answers:
                if answer.is_sus and answer.value:
                    self._non_empty += 1
        return self._non_empty

    def reset_non_empty_counter(self):
        self._non_empty = -1

    def add(self, answer: Answer):
        self.answers.append(answer)
        answer.row = self

    def empty(self):
        return all(answer.empty() for answer in self.answers)

    def to_string_list(self) -> List[str]:
        row = [self.timestamp]
        for answer in self.answers:
            row.append(answer.value)
        return row

    def __str__(self):
        return f'{self.timestamp}: {", ".join([answer.value for answer in self.answers])}'


class Question:
    def __init__(self, title: str, is_open: bool, is_sus: bool, auto_title: bool):
        self._title: str = title
        self.answers: List[Answer] = []
        self.is_open: bool = is_open
        self.is_sus: bool = is_sus
        self.auto_title: bool = auto_title
        self._counter = None

    @property
    def title(self):
        if not self.auto_title:
            return self._title
        top = self.counter.top_one()
        return top.label if top else self._title

    @property
    def counter(self):
        if self._counter is None:
            from counter import Counter
            self._counter = Counter()
            for answer in self.answers:
                self._counter.count_answer(answer)
        return self._counter

    def clear_counter(self):
        if self._counter is not None:
            self._counter = None

    def add(self, answer: Answer):
        self.answers.append(answer)
        answer.column = self

    def remove_answer(self, answer: Answer):
        self.answers.remove(answer)

    def top(self) -> List:
        return self.counter.top()

    def slugs_into_value(self, slug: str, value: str):
        for answer in self.answers:
            if answer.slug_name == slug:
                answer.value = value

    def __str__(self):
        return self.title
