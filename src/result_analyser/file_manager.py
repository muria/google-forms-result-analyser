import csv
from typing import List


class FileManager:
    @classmethod
    def open_csv(cls, path: str, newline=''):
        with open(path, newline=newline) as file:
            csv_file = csv.reader(file)
            rows = []
            for row in csv_file:
                rows.append(row)
            return rows

    @classmethod
    def save_csv(cls, path: str, data: List[List[str]]):
        with open(path, 'w', newline='') as file:
            writer = csv.writer(file)
            writer.writerows(data)
