import math


def count_digits(number: int):
    return int(math.log10(number)) + 1


def col_to_index(col: str):
    index = 0
    for i, letter in enumerate(col):
        letter = letter.upper()
        if not(ord('A') <= ord(letter) <= ord('Z')):
            raise f'Incorrect column selector: {col}'
        index *= 26**i
        index += ord(letter) - ord('A') + 1
    return index - 2  # minus one because A starts with 1, minus another one because we skip timestamp
