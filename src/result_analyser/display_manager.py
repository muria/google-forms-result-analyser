import math
import os
from enum import Enum

from getkey import getkey, keys
from typing import Sequence, Tuple

from utils import count_digits


class InputType(Enum):
    NONE = 0
    SINGLE_NUMBER = 1
    TWO_NUMBERS = 2
    ACTION = 3
    NUMBER_AND_ACTION = 4
    TEXT = 5

    def accepts_numbers(self) -> bool:
        return self.value in (1, 2, 4, 5)


class DisplayManager:
    @staticmethod
    def clear_screen():
        os.system("printf '\033c'")

    @staticmethod
    def text_rows(text: str):
        return math.ceil((len(text)) / os.get_terminal_size()[0])

    @staticmethod
    def _accept_enter(numbers: str, action: str, input_type: InputType, max_number: int) -> bool:
        if input_type == InputType.SINGLE_NUMBER:
            return not numbers or int(numbers) <= max_number
        elif input_type == InputType.TWO_NUMBERS:
            if not numbers:
                return True
            if ' ' not in numbers or numbers.endswith(' '):
                return False
            a, b = numbers.split(' ')
            return int(a) <= max_number and int(b) <= max_number
        elif input_type == InputType.NUMBER_AND_ACTION:
            if bool(numbers):
                return bool(action) and int(numbers) <= max_number
            else:
                return not action
        return True

    @classmethod
    def _input_loop(cls, header: str, items: Sequence = (), input_type: InputType = InputType.NONE,
                    numerate: bool = True, actions: str = '', default_input: str = '', cut: bool = True):
        position: int = 0
        base_input: str = default_input  # Holds text input, number or two numbers separated by space
        action: str = ''  # Holds single letter, marking action to be taken
        while True:
            cls.clear_screen()
            current_input = f'{base_input} {action}'.strip()
            header_text: str = f'{header}: {current_input}'
            text_rows: int = cls.text_rows(header_text)
            items_per_page: int = os.get_terminal_size()[1] - text_rows - 1
            last_visible_number: int = max(min(len(items), position + items_per_page), 1)
            item_digits: int = count_digits(last_visible_number)

            print(header_text)
            for i, item in enumerate(items[position: position + items_per_page], position + 1):
                if numerate:
                    item_line = f'{str(i).rjust(item_digits)}: {item}'
                else:
                    item_line = str(item)
                if len(item_line) > os.get_terminal_size()[0]:
                    item_line = item_line[:os.get_terminal_size()[0]]
                print(item_line)

            key: str = getkey()
            if key == keys.ENTER:
                if cls._accept_enter(base_input, action, input_type, len(items)):
                    if input_type == InputType.NONE:
                        return
                    if input_type == InputType.SINGLE_NUMBER:
                        return int(base_input) if base_input else 0
                    if input_type == InputType.TWO_NUMBERS:
                        if not base_input:
                            return 0, 0
                        a, b = base_input.split(' ')
                        return int(a), int(b)
                    if input_type == InputType.ACTION:
                        return action
                    if input_type == InputType.NUMBER_AND_ACTION:
                        return (int(base_input), action) if base_input else (0, '')
                    if input_type == InputType.TEXT:
                        return base_input
            elif key == keys.DOWN:
                position += 1
            elif key == keys.UP:
                position -= 1
            elif key == keys.HOME:
                position = 0
            elif key.isdigit():
                if input_type.accepts_numbers():
                    base_input += key
            elif key in actions:
                action = key
            elif key == keys.SPACE:
                if (input_type == InputType.TWO_NUMBERS and base_input and ' ' not in base_input) or (input_type == InputType.TEXT):
                    base_input += ' '
            elif key == keys.BACKSPACE:
                if action:
                    action = ''
                else:
                    base_input = base_input[:-1]
            elif key.isprintable():
                if input_type == InputType.TEXT:
                    base_input += key

            if position < 0:
                position = 0
            if position >= len(items):
                position = len(items) - 1

    @classmethod
    def message(cls, header: str, rows: Sequence[str]):
        return cls._input_loop(header, rows, InputType.NONE, False)

    @classmethod
    def simple_choice(cls, text: str, options: str) -> str:
        return cls._input_loop(f'{text} [{"/".join(options)}]', input_type=InputType.ACTION, actions=options)

    @classmethod
    def simple_choice_with_list(cls, text: str, items: Sequence, options: str, auto_options: bool = True,
                                numerate: bool = True) -> str:
        if auto_options:
            text = f'{text} [{"/".join(options)}]'
        return cls._input_loop(text, items, InputType.ACTION, actions=options, numerate=numerate)

    @classmethod
    def text_input(cls, prompt: str, default: str):
        return cls._input_loop(prompt, input_type=InputType.TEXT, default_input=default)

    @classmethod
    def pick_action(cls, actions: Sequence) -> int:
        return cls._input_loop('Pick an action', actions, InputType.SINGLE_NUMBER)

    @classmethod
    def pick_item(cls, text: str, items: Sequence, cut: bool = True):
        return cls._input_loop(text, items, InputType.SINGLE_NUMBER, cut=cut)

    @classmethod
    def pick_action_with_item(cls, text: str, actions: str, items: Sequence) -> Tuple[int, str]:
        return cls._input_loop(text, items, InputType.NUMBER_AND_ACTION, actions=actions)

    @classmethod
    def pick_two_items(cls, header: str, items: Sequence) -> Tuple[int, int]:
        return cls._input_loop(header, items, InputType.TWO_NUMBERS)
