import operator
from typing import Sequence, List, Dict, Optional

from entry import Entry, Answer, Question


class Collection:
    def __init__(self):
        self.count: int = 0
        self.answers: List[Answer] = []
        self.labels: Dict[str, int] = {}
        self.top_label: Optional[str] = None

    def count_answer(self, answer: Answer):
        self.count += 1
        self.answers.append(answer)
        if answer.value in self.labels:
            self.labels[answer.value] += 1
        else:
            self.labels[answer.value] = 1
        if self.top_label and self.top_label != answer.value:
            self.top_label = None

    @property
    def label(self):
        if self.top_label is None:
            max_item = max(self.labels.items(), key=lambda item: item[1])
            self.top_label = max_item[0]
        return self.top_label

    @property
    def slug(self) -> Optional[str]:
        if self.answers:
            return self.answers[0].slug_name

    def __str__(self):
        return f'{self.count}, {self.label}'

    def top_labels(self) -> List:
        return sorted(self.labels.items(), key=operator.itemgetter(1), reverse=True)

    def sources_for_label(self, label: str) -> List[Question]:
        sources = set()
        for answer in self.answers:
            if answer.value == label:
                sources.add(answer.column)
        return list(sources)

    def merge_labels(self, source: str, destination: str):
        for answer in self.answers:
            if answer.value == 'source':
                answer.value = destination
        taken = self.labels[source]
        del self.labels[source]
        self.labels[destination] += taken
        if self.top_label and self.top_label != destination:
            self.top_label = None

    def clear_all(self):
        for answer in self.answers:
            answer.value = ''
        self.labels = {'': len(self.answers)}
        self.top_label = ''


class Counter:
    def __init__(self):
        self.state: Dict[str, Collection] = {}
        self._sorted = None

    def count_entries(self, entries: Sequence[Entry], column: int = -1, open_only: bool = False):
        for entry in entries:
            if column != -1:
                self.count_answer(entry[column], open_only)
            else:
                for answer in entry:
                    self.count_answer(answer, open_only)

    def count_answer(self, answer: Answer, open_only: bool = False):
        if answer.empty() or open_only and not answer.is_open:
            return
        self._sorted = None
        if answer.slug_name in self.state:
            col = self.state[answer.slug_name]
        else:
            col = Collection()
            self.state[answer.slug_name] = col
        col.count_answer(answer)

    def top_one(self) -> Optional[Collection]:
        top: Optional[Collection] = None
        for slug, collection in self.state.items():
            if top is None or collection.count > top.count:
                top = collection
        return top

    def top(self) -> List[Collection]:
        if self._sorted is None:
            self._sorted = sorted(self.state.values(), key=lambda a: a.count, reverse=True)
        return self._sorted.copy()

    def slugs_into_value(self, slug: str, value: str):
        collection: Collection = self.state[slug]
        for answer in collection.answers:
            answer.value = value
        del self.state[slug]
        self._sorted = None
        for answer in collection.answers:
            self.count_answer(answer)

    def update(self, slug: str):
        collection: Collection = self.state[slug]
        del self.state[slug]
        self._sorted = None
        for answer in collection.answers:
            self.count_answer(answer)

    def __getitem__(self, item):
        return self.state[item]
