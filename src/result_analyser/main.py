import sys

from csv_analyser import CsvAnalyser
from file_manager import FileManager


def main():
    if len(sys.argv) != 2:
        print(f'Usage: {sys.argv[0]} <input_file.csv>')
        sys.exit(1)

    in_name = sys.argv[1]
    analyser = CsvAnalyser(in_name)
    analyser.start()


if __name__ == '__main__':
    main()
