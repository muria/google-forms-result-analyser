from actions import ActionMenu, MostCommonInAll, SaveCSV, SelectQuestion, SelectEntry, SusEntries
from csv_file import CsvFile


class CsvAnalyser:
    def __init__(self, input_file_name: str):
        self.csv_file: CsvFile = CsvFile.open_from_path(input_file_name, open_questions='C-BZ', sus_questions='C-BZ',
                                                        auto_title='C-BZ')

    def start(self):
        action_menu = ActionMenu([
            MostCommonInAll(self.csv_file),
            SelectEntry(self.csv_file),
            SusEntries(self.csv_file),
            SelectQuestion(self.csv_file),
            SaveCSV(self.csv_file)
        ])
        action_menu.start()
