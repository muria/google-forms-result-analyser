# Result Analyser

This is application I made to make analyzing data from certain Google Forms survey easier.

Application was fully created in two days, and I probably won't need new use cases, so I'm not exactly aiming for it to be perfect and don't really explain much how it works.

The main goal behind this project is to group data from Google Forms, especially when working with open questions. For example to treat answer with typo the same as correctly written one. It's also good for finding and removing garbage answers. If you think this could help you and want me to explain how to use the application or how it works, just open issue on GitLab.